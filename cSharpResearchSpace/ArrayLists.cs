﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharpResearchSpace
{
    class ArrayLists
    {
       

        public ArrayLists()
        {

            ArrayList testArrayList = new ArrayList(100);

            testArrayList.Add("number 25");
            testArrayList.Add(26);
            testArrayList.Add(27);
            testArrayList.Add(28);
            testArrayList.Add("number 29");
            testArrayList.Add(30);
            testArrayList.Add("number 31");
            testArrayList.Add("number 32");


            Console.WriteLine(testArrayList.Count);

            double sum = 0;

            foreach (object obj in testArrayList)
            {
                if (obj is int)
                {
                    sum += Convert.ToDouble(obj);
                }else if(obj is double)
                {
                    sum += (double)obj;
                }else if(obj is string)
                {
                    Console.WriteLine(obj);
                }
            }
            Console.WriteLine(sum);



            List<string> names = new List<string>();

            names.Add(Console.ReadLine());
            names.Add(Console.ReadLine());
            names.Add(Console.ReadLine());
            names.Add(Console.ReadLine());



            foreach (string name in names)
            {

                Console.WriteLine(" each name stored in array = {0} ", name);
            

            }

            for (int i = 0; i < names.Count; i++)
            {
                string s = names[i];
                Console.WriteLine(s);
            }



        }
    }


}
