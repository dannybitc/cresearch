﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace cSharpResearchSpace
{
    class ThreadTests
    {

        public class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public string Sex { get; set; }

            public Person(string name, int age, string sex)
            {
                this.Name = name;
                this.Age = age;
                this.Sex = sex;
            }
        }
        public ThreadTests()
        {
            Thread workerThread = new Thread(new ThreadStart(Print));
            // Start secondary thread  
            workerThread.Start();

            // Main thread : Print 1 to 10 every 0.2 second.   
            // Thread.Sleep method is responsible for making the current thread sleep  
            // in milliseconds. During its sleep, a thread does nothing.  
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"Main thread: {i}");
                Thread.Sleep(200);
            }

            Console.ReadKey();


           static void Print()
                {
                    for (int i = 11; i < 20; i++)
                    {
                        Console.WriteLine($"Worker thread: {i}");
                        Thread.Sleep(1000);
                    }
                }



            static void BackgroundTask(Object stateInfo)
            {
                Console.WriteLine("Hello! I'm a worker from ThreadPool");
                Thread.Sleep(1000);
            }

            static void BackgroundTaskWithObject(Object stateInfo)
            {
                Person data = (Person)stateInfo;
                Console.WriteLine($"Hi {data.Name} from ThreadPool.");
                Thread.Sleep(1000);
            }

            ThreadPool.QueueUserWorkItem(BackgroundTask);
            Console.WriteLine("Main thread does some work, then sleeps.");
            Thread.Sleep(500);


            Person p = new Person("Daniel", 40, "Male");
            ThreadPool.QueueUserWorkItem(BackgroundTaskWithObject, p);

            Console.ReadKey();

            int workers, ports;

            // Get maximum number of threads  
            ThreadPool.GetMaxThreads(out workers, out ports);
            Console.WriteLine($"Maximum worker threads: {workers} ");
            Console.WriteLine($"Maximum completion port threads: {ports}");

            // Get available threads  
            ThreadPool.GetAvailableThreads(out workers, out ports);
            Console.WriteLine($"Availalbe worker threads: {workers} ");
            Console.WriteLine($"Available completion port threads: {ports}");

            // Set minimum threads  
            int minWorker, minIOC;
            ThreadPool.GetMinThreads(out minWorker, out minIOC);
            ThreadPool.SetMinThreads(4, minIOC);

            // Get total number of processes availalbe on the machine  
            int processCount = Environment.ProcessorCount;
            Console.WriteLine($"No. of processes available on the system: {processCount}");

            // Get minimum number of threads  
            ThreadPool.GetMinThreads(out workers, out ports);
            Console.WriteLine($"Minimum worker threads: {workers} ");
            Console.WriteLine($"Minimum completion port threads: {ports}");

            Console.ReadKey();



        }

 
    }


 }

