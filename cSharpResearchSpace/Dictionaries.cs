﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharpResearchSpace
{
    internal class Dictionaries
    {



        public class Employee { 
            public string Role { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
        
            public float Rate { get; set; }

            public float Salary
                {
                    get { return Rate * 8 * 5 * 4 * 12; }
                }

            public Employee(string role,string name,int age,float rate)
                {
                    Role = role;
                    Name = name;
                    Age = age;
                    Rate = rate;
                }

         }


        public Dictionaries()
        {
            Employee[] employees =
            {
                new Employee("CEO","Jane",55,200),
                new Employee("MANAGER","John",45,150),
                new Employee("LEAD DEV","Daniel",35,120),
                new Employee("LEAD IT","Lucy",37,100),
                new Employee("JUNIOR DEV","Tim",25,80),
                new Employee("DEV OPs","Sarah",40,130),
                new Employee("SENIOR DATABASE ADMIN","Mary",36,126),
            };

            //Dictionary<int,string> employeeDictionary = new Dictionary<int,string>();

            Dictionary<string,Employee> employeesDirectory = new Dictionary<string,Employee>();////.........create dictionay

            foreach (Employee employee in employees)//......add employees to dictionary
            {
                employeesDirectory.Add(employee.Role, employee);
            }
            //.......update dictionary...ADD
            string keyToUpdate = "MANAGER";
            if(employeesDirectory.ContainsKey(keyToUpdate))
            {
                employeesDirectory[keyToUpdate] = new Employee("MANAGER", "Abby", 27, 150);
                Console.WriteLine("Employee with Role/Key {0} was updated", keyToUpdate);
            }
            else
            {
                Console.WriteLine("employee not found");
            }
            // update dictionat....remove
            string keyToRemove = "JUNIOR DEV";
            if (employeesDirectory.Remove(keyToRemove))
            {
             
                Console.WriteLine("Employee with Role/Key {0} was Removed", keyToUpdate);
            }
            else
            {
                Console.WriteLine("employee not found");
            }


            for (int i = 0; i < employeesDirectory.Count; i++)/// interate through dictionary
            {
                // using Element(i) to return the key-value pair stored at index i
                KeyValuePair<string, Employee> keyValuePair = employeesDirectory.ElementAt(i);
                // print the key

                Console.WriteLine("Key: {0}, i is {1}", keyValuePair.Key, i);
                // storing the value in an employee object

                Employee employeeValue = keyValuePair.Value;
                // printing the values of the employee object
                Console.WriteLine("Employee Name: {0}", employeeValue.Name);
                Console.WriteLine("Employee Role: {0}", employeeValue.Role);
                Console.WriteLine("Employee Age: {0}", employeeValue.Age);
                Console.WriteLine("Employee Salary: {0}", employeeValue.Salary);
               
            }


            Employee empl = employeesDirectory["CEO"];

            Console.WriteLine("Employee Name : {0}, Role: {1}, Salary: {2}", empl.Name, empl.Role, empl.Salary);

        }




    }
}
