﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharpResearchSpace
{
    class ArraysParameters
    {
   

    public ArraysParameters()
    {
            int[] studentGrades = new int[] { 14, 13, 8, 12, 16 };
            double averageResult = GetAverage(studentGrades);

            foreach (int grade in studentGrades)
            {
                Console.WriteLine(" {0}" ,grade);
            }

            Console.WriteLine("The average is:{0}", averageResult);

            int min = MinVal(-1873, 2002, 4456, -10);

            Console.WriteLine("The min is :{0}", min);

            int sort = SortGradeAlgo(78, 34, 25, 74, 8, 21, 17, 5, 23, 89);

            Console.WriteLine(SortGradeAlgo(sort));


        }

    static double GetAverage(int[] gradesArray)/////.......................build a method that accepys an Array as a argument(studentGrades) which is the parameter
        {
            int size = gradesArray.Length;
            double average;
            int sum = 0;

            for (int i = 0; i < size; i++)
            {
                sum += gradesArray[i];
            }

            average = (double)sum / size;
            return average;
        }


        static int MinVal(params int[] numbers)///........................params key word allows you to pass as many parameters as needed
        {
            int min = int.MaxValue;

            foreach (int number in numbers)
            {
                if (number < min)
                    min = number;

            }
            return min;

        }


        static int SortGradeAlgo(params int[] sorted)
        {
            var sort = sorted.Length;

            var itemMoved = false;

            do
            {
                itemMoved = false;
                for (int i = 0; i < sorted.Count() - 1; i++)
                {
                    if (sorted[i] > sorted[i + 1])
                    {
                        var lowerValue = sorted[i + 1];
                        sorted[i + 1] = sorted[i];
                        sorted[i] = lowerValue;
                        itemMoved = true;
                    }
                }
            } while (itemMoved);
            Console.WriteLine("The Sorted Array :");
            foreach (int aray in sorted)
                Console.Write(aray + " ");
            Console.ReadLine();
            return sort;
        }

}
}