﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharpResearchSpace
{
    internal class Queues
    {

        public class Order
        {
            public int OrderId { get; set; }

            public int OrderQauntity { get; set; }

            public Order(int id, int orderQuanttity) { OrderId = id; OrderQauntity = orderQuanttity;}

            public void ProcessOrder() { Console.WriteLine($"Order....{OrderId } has been processed!."); }

          
        }


        static Order[] RecieveOrderFromBranch1()
        {
            Order[] orders = new Order[]
            {
                new Order(3,5),
                new Order(4,5),
                new Order(5,10),
                new Order(6,8),
            };
            return orders;
        }

        static Order[] RecieveOrderFromBranch2()
        {
            Order[] orders = new Order[]
            {
                new Order(1,5),
                new Order(2,4),
                new Order(5,13),
                new Order(6,18),
            };
            return orders;
        }

        public Queues()
        {

            Queue<Order> ordersOueue = new Queue<Order>();

            foreach (Order o in RecieveOrderFromBranch1())
            {
                ordersOueue.Enqueue(o);
            }

            foreach (Order o in RecieveOrderFromBranch2())
            {
                ordersOueue.Enqueue(o);
            }

            while (ordersOueue.Count > 0)
            {
                Order currentOrder = ordersOueue.Dequeue();

                currentOrder.ProcessOrder();
            }

        }

       

    }
}
