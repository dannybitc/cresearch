﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharpResearchSpace
{
    class ForEach
    {
        public ForEach() {
        int[] nums = new int[10];

        for(int i = 0; i < 10; i++)
            {
                nums[i] = i + 1;
            }
        

        for(int j = 0; j < nums.Length; j++)
            {
                Console.WriteLine("Element FOR {0} = {1}", j, nums[j]);
            }

            int counter = 0;
            foreach(int k in nums)
            {
                Console.WriteLine("Element LOOP {0} = {1}", counter, k);
                counter++;
            }
           
            Console.ReadKey();
         }

    }
}
