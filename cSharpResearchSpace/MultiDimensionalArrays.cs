﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharpResearchSpace
{
    class MultiDimensionalArrays
    {

      public MultiDimensionalArrays()
        {   
            // declare 2D Array
            string[,] matrix;

            // 3D array
            int[,,] threeD;

            //............................two dimensional array

            int[,] array2D = new int[,]
            {
                { 1,2,3},
                { 4,5,6},
                { 7,8,9}
            };

            Console.WriteLine("The value is {0}", array2D[1,1]);////index acces


            //..............................two dimensional array which declares rank
             string[,] array2DString = new string[3, 2]//...rank
            {
                { "one", "two" },
                {"three", "four" },
                {"five", "six" }
            };


            array2DString[1, 1] = "changed element";

            int dimensions = array2DString.Rank;

            Console.WriteLine("The value is {0}", array2DString[1, 1]);

            Console.WriteLine("The dimension is {0}", dimensions);


            //..............................pre-initialized array two dimensional array

            int[,] array2D2 = { { 1, 2 }, { 3, 4 } };


            //................................... three dimensional array
            string[,,] array3D = new string[,,]
            {///1
                {///2
                    {"000","001" },///3
                    {"010","011" }
                },
                {
                    {"100","101" },
                    {"110", "111" }
                }
            };

            Console.WriteLine("The value is {0}", array3D[1, 1, 0 ]);////index access


            //.................................nested for loops and 2d arrays, loop twice to find even numbers in array

            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (array2D[i, j] % 2 == 0)
                        Console.Write(array2D[i, j] + " " );
                    else
                        Console.WriteLine(" ");
                }

            }


            ///.........................................print diagonal values, 
            Console.WriteLine("This is the diagonal values");
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (i == j)
                        Console.Write(array2D[i, j] + " ");
                    else
                        Console.WriteLine(" ");
                }
                Console.WriteLine("");
            }

            Console.WriteLine("This is the diagonal values in one loop");

            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                Console.WriteLine(array2D[i, i]);
            }



            Console.WriteLine("This is the revers diagonal values in one loop");
            //...two count variables, i start from the left and count inwards, j starts from the right and count backwards
            for (int i = 0, j = 2; i < array2D.GetLength(0); i++ ,j--)
            {
                Console.WriteLine(array2D[i, j]);
            }
            
            Console.ReadKey();

            //.....................................................JAGGED ARRAYS

            int[][] jaggedArray = new int[][] //............Declare and initialize array
            {
                new int[]{2,3,4,5,6},
                new int[]{7,8,9,10,11}
            };

            for (int i = 0; i < jaggedArray.Length; i++) ///////................nested for loop needed to read all element in multi-array
            {
                Console.WriteLine("Elemnt {0}", i);
                for (int j = 0; j < jaggedArray[i].Length; j++)
                    Console.WriteLine("{0}", jaggedArray[i][j]);

            }

            Console.ReadKey();



       }


    }
}
