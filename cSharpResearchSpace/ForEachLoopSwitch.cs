﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharpResearchSpace
{
    class ForEachLoopSwitch
    {

        public ForEachLoopSwitch()
        {
        

            Boolean valid = false;
            string inputValueType;

            Console.Write("Enter a value :");
            string inputValue = Console.ReadLine();

            Console.WriteLine("Select the Data type to validate the input you have entered");
            Console.WriteLine("Press 1 for the string");
            Console.WriteLine("Press 2 for Integar");
            Console.WriteLine("Press 3 for Boolean");

            Console.WriteLine("Enter :");
            int inputType = Convert.ToInt32(Console.ReadLine());

            switch (inputType)
            {
                case 1:

                    valid = isAllAlphabetic(inputValue);
                    inputValueType = "String";
                    break;

                case 2:

                    int retValue = 0;

                    valid = int.TryParse(inputValue, out retValue);
                    inputValueType = "Integer";
                    break;

                case 3:

                    bool redFlag = false;

                    valid = bool.TryParse(inputValue, out redFlag);
                    inputValueType = "Boolean";
                    break;

                default:
                    inputValueType = "unknown";
                    Console.WriteLine("Not able to detect input type");
                    break;



            }
            Console.WriteLine("You have entered a value : {0}", inputValue);
            if (valid)
            {
                Console.WriteLine("It is valid : {0}", inputValueType);
            }
            else
	        {
                Console.WriteLine("It is an valid : {0}", inputValueType );
	        }
          
        }

        static bool isAllAlphabetic(string value)
        {
            foreach (char c in value)
            {
                if (!char.IsLetter(c))
                    return false;
            }
            return true;
        }




    }

}
