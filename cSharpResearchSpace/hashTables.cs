﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace cSharpResearchSpace
{
    class hashTables
    {



        public class Student
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public float GPA { get; set; }

            public Student(int id, string name, float GPA)
            {
                this.Id = id;
                this.Name = name;
                this.GPA = GPA;
            }

        }

        public hashTables() { 

        Hashtable studentsTable = new Hashtable();


        Student stu1 = new Student(1, "Maria", 98);
        Student stu2 = new Student(2, "Daniel", 3000);
        Student stu3 = new Student(3, "Len", 79);
        Student stu4 = new Student(4, "Sergio", 89);
        Student stu5 = new Student(5, "Abby", 100);

            studentsTable.Add(stu1.Id, stu1);
            studentsTable.Add(stu2.Id, stu2);
            studentsTable.Add(stu3.Id, stu3);
            studentsTable.Add(stu4.Id, stu4);
            studentsTable.Add(stu5.Id, stu5);

            Student storeStudent1 = (Student)studentsTable[stu1.Id];

            Console.WriteLine("Student ID:{0}, Name:{1}, GPA{2}", storeStudent1.Id, storeStudent1.Name, storeStudent1.GPA);

            foreach (DictionaryEntry entry in studentsTable)
            {
                Student temp = (Student)entry.Value;
                Console.WriteLine("Student ID:{0}", temp.Id);
                Console.WriteLine("Student Name:{0}", temp.Name);
                Console.WriteLine("Student GPA:{0}", temp.GPA);
               
            }

            foreach (Student value in studentsTable.Values)
            {
              
                Console.WriteLine("Student ID:{0}", value.Id);
                Console.WriteLine("Student Name:{0}", value.Name);
                Console.WriteLine("Student GPA:{0}", value.GPA);

            }
        }

    }
}
